# vbus.java

vBus - Java Client
==================

A Java client for vBus, VeeaHub Framework.

#Pre-requisites

vBus Java client requires libunixsocket-java library.

```groovy
apt-get install libunixsocket-java
```

#Installation

vbus.java client comes as a jar file: `java-vbus.jar` build using Gradle.

You can either use the already build Jar file in `Release/`

Or you can build it again:

```groovy
./gradlew build
```
Jar file will be located in `build/libs/``

Once you have the `java-vbus.jar`, put it in your project, in the Libs folder.

#Usage
##Import in your project
Add at the botton of your java file:

```groovy
import vbus.vBusClient;
```

##Connecting

vBus client will automatically discover the vBus system on the local network.

1. Create a client instanceThe and provide a unique name.

```groovy
vBusClient client = new vBusClient("mycompany.myapplication");
```

For your information, client creation will generate a configuration file in your system.
Per default this file will be located in `$HOME`.
You can choose to place it somewhere else, providing the ENV value `export VBUS_PATH=/var/lib/veea/`

2. Request connection
```groovy
boolean success = client.connect();
if(success == false)
{
    System.out.format("Unable to set up vbus");
    System.exit(1);
} 
```

##Request priviledges

In order to access specific capabilities in the vBus system, you will need to request priviledges.
For instance, in order to access Zigbee devices, request:

```groovy
client.Permission_Subscribe("system.zigbee.>");
client.Permission_Publish("system.zigbee.>");
```

##Publishing

Once connected, you can start communication.
Publishing action can be performed this way:

```groovy
client.Publish("subject", "message");
```

##Request

Request are publish function that handles a reply.
Since getting a response may take some time, `150000`is the timeout value (in ms).

```groovy
String newmessage = client.Request("subject", "", 15000);
System.out.println(newmessage);
```

Accessing the vBus element list is also a request, that has it's own function.
The return String value is a json array including all the element matching the filter requested.

1. Getting all the elements (no filter)
```groovy
String elementList = client.List("");
``` 
2. Getting all the zigbee devices (filter "bridge" vaue)
```groovy
String zigbeeList = client.List("{'bridge':'system.zigbee'}");
``` 

##Subscribe

Your application can also request to listen to specific subject.
As an information (not used for now), we define the value format expected.

```groovy
client.Subscribe("subject", "integer", (msg) -> {
    System.out.println(msg);
});
```

##Example

Here is a full example to access the temperature value of a Zigbee Philips Hue Proximity Sensor:

```groovy
vBusClient client = new vBusClient("mycompany.myapplication");

boolean success = client.connect();
if(success == false)
{
    System.out.format("Unable to set up vbus");
    System.exit(1);
} 

client.Permission_Subscribe("system.zigbee.>");
client.Permission_Publish("system.zigbee.>");

String zigbeeList = client.List("{'bridge':'system.zigbee'}");
System.out.println("Zigbee device list in the Mesh: \n" + zigbeeList);

String deviceDetails = client.Request("system.db.getelement", "{'uuid':'00:17:88:01:04:b5:53:2d'}");
System.out.println("Device 00:17:88:01:04:b5:53:2d details: \n" + deviceDetails);

String newvalue = client.Request("system.zigbee.00:17:88:01:04:b5:53:2d.2.1026.0.get", "", 15000);
System.out.println("My temperature is: " + newvalue);
``` 
