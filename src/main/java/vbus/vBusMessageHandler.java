package vbus;

public interface vBusMessageHandler {
    void onMessage(String msg) throws InterruptedException;
}
