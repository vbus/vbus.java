package io.veea.VeeaHub;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.annotations.IntrospectionDescription;
import org.freedesktop.dbus.annotations.MethodNoReply;
import org.freedesktop.dbus.interfaces.DBusInterface;
import org.freedesktop.dbus.types.UInt32;

public interface Info extends DBusInterface {
    String ActiveSystem();
    Integer BatteryIsGood();
    String BoardType();
    Integer BootCount();
    UInt32  BootCrc();
    UInt32 BootMagic();
    Integer[] Cpu();
    Integer CpuTemperature();
    String HardwarePcba();
    String[] HardwareModuleInventory();
    String HardwareType();
    String HardwareRevision();
    String Hostname();
    int LastBootCount();
    Map<Integer, String> LastResetReason();
    Map<Integer, String> LastSystemState();
    Map<Integer, String> LastRebootTime();
    Map<Integer, String> LastRecoveryTime();
    String[] LoadAvg();
    String ManufacturerSerial();
    String[] Memory();
    String Model();
    String NodeType();
    String[] PlatformSoftware();
    String[] ProductSerial();
    String[] ProductFeatures();
    Map<Integer, String> RequestSystem();
    Map<Integer, String> ResetReason();
    Map<Integer, String> RunningSystem();
    String SocSerial();
    String[] StorageCapacity();
    Map<Integer, String> SystemState();
    String VeeaSerial();
}

